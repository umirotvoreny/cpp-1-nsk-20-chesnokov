#include "CRC32.h"
#include <vector>

namespace {
uint32_t CRC32ForByte(uint32_t r)
{
	for (int j = 0; j < 8; ++j)
		r = (r & 1 ? 0 : static_cast<uint32_t>(0xEDB88320L)) ^ r >> 1;
	return r ^ static_cast<uint32_t>(0xFF000000L);
}

void CRC32(const void* data, size_t nBytes, uint32_t* crc)
{
	static uint32_t table[0x100];
	if (!*table)
		for (size_t i = 0; i < 0x100; ++i)
			table[i] = CRC32ForByte(i);
	for (size_t i = 0; i < nBytes; ++i)
		*crc = table[(uint8_t)*crc ^ ((uint8_t*)data)[i]] ^ *crc >> 8;
}

}
uint32_t GetHashCRC32(std::istream& inputStream)
{
	std::vector<char> buffer;
	buffer.resize(1 << 15);
	uint32_t crc = 0;

	while (!inputStream.eof() && !inputStream.fail())
	{
		inputStream.read(buffer.data(), buffer.size());
		CRC32(buffer.data(), inputStream.gcount(), &crc);
	}

	return crc;
}