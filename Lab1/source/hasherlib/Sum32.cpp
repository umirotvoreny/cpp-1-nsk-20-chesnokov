#include "Sum32.h"

uint32_t GetHashSum32(std::istream& inputStream)
{
	uint32_t hash = 0;
	int byteCount = 0;
	uint32_t fourBytes = 0;
	char oneByte = 0;

	while (true)
	{
		if (byteCount == 4)
		{
			hash += fourBytes;
			fourBytes = 0;
			byteCount = 0;
		}

		inputStream.get(oneByte);

		if (inputStream.eof())
		{
			hash += fourBytes;
			break;
		}

		fourBytes = fourBytes << 8;
		fourBytes += *reinterpret_cast<uint8_t*>(&oneByte);
		++byteCount;
	}

	return hash;
}
