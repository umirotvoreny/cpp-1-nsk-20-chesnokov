#include <iostream>
#include "hasherlib/Sum32.h"
#include "hasherlib/CRC32.h"
#include <fstream>
#include <string>
#include <vector>

enum class State
{
	InvalidInput,
	Help,
	SuccessInput
};

struct Parameters
{
	std::string filename;
	std::string algorithm;
};

State ParseParameters(const std::vector<std::string>& args, Parameters& parameters)
{
	if (args.size() == 2)
	{
		if(args[1] == "-h")
		{
			return State::Help;
		}
		else
		{
			return State::InvalidInput;
		}
	}
	else if (args.size() != 4)
	{
		return State::InvalidInput;
	}
	else
	{
		int modePosition = 1;

		for (; modePosition < 3; ++modePosition)
		{
			if (args[modePosition] == "-m")
			{
				break;
			}
		}

		if (modePosition == 1)
		{
			parameters.algorithm = args[2];
			parameters.filename = args[3];
		}
		else if (modePosition == 2)
		{
			parameters.algorithm = args[3];
			parameters.filename = args[1];
		}
		else
		{
			return State::InvalidInput;
		}
	}

	if (parameters.algorithm != "Sum32" && parameters.algorithm != "CRC32")
	{
		return State::InvalidInput;
	}

	return State::SuccessInput;
}

int main(int argc, char* argv[])
{
	std::vector<std::string> args;
	Parameters parameters;

	for (int i = 0; i < argc; ++i)
	{
		args.push_back(argv[i]);
	}

	State state = ParseParameters(args, parameters);

	if (state == State::Help)
	{
		std::cout << ".h/hasher -m <mode> <filename>\tor<<\t.h/hasher <filename> -m <mode>" << std::endl;
		std::cout << "<filename> - any, <mode> - Sum32 or CRC32" << std::endl;
		return 0;
	}
	else if (state == State::InvalidInput)
	{
		
		std::cout << ".h/hasher -m <mode> <filename>\tor<<\t.h/hasher <filename> -m <mode>" << std::endl;
		std::cout << "<filename> - any, <mode> - Sum32 or CRC32" << std::endl;
		return 1;
	}
	else if (state == State::SuccessInput)
	{
		std::ifstream inputStream(parameters.filename, std::ios::binary);

		if (!inputStream)
		{
			std::cerr << "Wrong file name!" << std::endl;
			return 1;
		}

		if (parameters.algorithm == "Sum32")
		{
			std::cout << "Sum32 hash is: " << GetHashSum32(inputStream) << std::endl;
		}
		else
		{
			std::cout << "CRC32 hash is: " << GetHashCRC32(inputStream) << std::endl;
		}

		inputStream.close();
	}
	else
	{
		return 42;
	}

	return 0;
}

