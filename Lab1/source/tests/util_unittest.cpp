#include "gtest/gtest.h"

#include "../hasherlib/CRC32.h"
#include "../hasherlib/Sum32.h"

#include <sstream>

TEST(CalcHashSum32, emptyFile)
{
	std::stringstream empty("");
	EXPECT_EQ(0, GetHashSum32(empty));
}

TEST(CalcHashSum32, 1byteFileASymbol)
{
	std::stringstream a("a");
	EXPECT_EQ(97, GetHashSum32(a));
}

TEST(CalcHashSum32, alphabet)
{
	std::stringstream alphabet("abcdefghijklmnopqrstuvwxyz");
	EXPECT_EQ(2223704590, GetHashSum32(alphabet));	
}

TEST(CalcHashCRC32, 1byteFileASymbol)
{
	std::stringstream a("a");
	EXPECT_EQ(3904355907, GetHashCRC32(a));
}

TEST(CalcHashCRC32, alphabet)
{
	std::stringstream alphabet("abcdefghijklmnopqrstuvwxyz");
	EXPECT_EQ(1277644989, GetHashCRC32(alphabet));	
}
