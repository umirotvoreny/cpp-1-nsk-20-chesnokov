#include "Circle.h"
#include <cmath>

Circle::Circle(int id, Color color, Point point, double radius) : Shape(id, color, point), radius(radius)
{
	if (radius < 0)
	{
		throw std::exception("Radius should be positive");
	}
}

void Circle::Scale(double scale)
{
	if(scale < 0)
	{
		throw std::exception("Wrong scale size");
	}
	
	radius *= scale;
}

bool Circle::ContainsPoint(Point otherPoint) const
{
	return pow(otherPoint.GetX() - GetPoint().GetX(), 2) + pow(otherPoint.GetY() - GetPoint().GetY(), 2) <= pow(radius, 2);
}
