#pragma once

#include <exception>
#include"Shape.h"

class Circle : public Shape
{
private:
	double radius;
	
public:
	Circle() = default;
	Circle(int id, Color color, Point point, double radius);	
	void Scale(double scale) override;
	bool ContainsPoint(Point otherPoint) const override;
};

