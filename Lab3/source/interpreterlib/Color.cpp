#include "Color.h"
#include <exception>

Color::Color(int red, int green, int blue) : red(red), green(green), blue(blue)
{
	if(red < 0 || red > 255)
	{
		throw std::exception("Wrong red color");
	}
	else if(green < 0 || green > 255)
	{
		throw std::exception("Wrong green color");
	}
	else if (blue < 0 || blue > 255)
	{
		throw std::exception("Wrong blue color");
	}
}

int Color::GetRed() const
{
	return red;
}

int Color::GetGreen() const
{
	return green;
}

int Color::GetBlue() const
{
	return blue;
}
