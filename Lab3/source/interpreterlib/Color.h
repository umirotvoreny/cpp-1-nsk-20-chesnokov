#pragma once

class Color
{
private:
	int red;
	int green;
	int blue;
public:
	Color() = default;
	Color(int red, int green, int blue);
	int GetRed() const;
	int GetGreen() const;
	int GetBlue() const;
};

