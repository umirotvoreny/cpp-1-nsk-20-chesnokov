#include "Interpreter.h"
#include "Circle.h"
#include "Triangle.h"
#include "Square.h"
#include "Rectangle.h"
#include "Color.h"
#include "Scene.h"
#include <utility>
#include <sstream>

void Interpreter::ExecuteCommand(const std::vector<std::string>& commandLine)
{
	if (commandLine.empty())
	{
		return;
	}

	if (commandLine[0] == "create")
	{
		CreateShape(commandLine);
	}
	else if (commandLine[0] == "set")
	{
		SetColor(commandLine);
	}
	else if (commandLine[0] == "move")
	{
		Move(commandLine);
	}
	else if (commandLine[0] == "scale")
	{
		Scale(commandLine);
	}
	else if (commandLine[0] == "bring")
	{
		BringTo(commandLine);
	}
	else if (commandLine[0] == "draw")
	{
		Draw(commandLine);
	}
	else
	{
		throw std::exception("Wrong first command");
	}
}

void Interpreter::CreateShape(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() <= 1)
	{
		throw std::exception("Wrong command create");
	}

	if (commandLine[1] == "circle")
	{
		CreateCircle(commandLine);
	}
	else if (commandLine[1] == "rectangle")
	{
		CreateRectangle(commandLine);
	}
	else if (commandLine[1] == "square")
	{
		CreateSquare(commandLine);
	}
	else if (commandLine[1] == "triangle")
	{
		CreateTriangle(commandLine);
	}
	else
	{
		throw std::exception("Wrong second word command");
	}
}

void Interpreter::CreateCircle(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 9)
	{
		throw std::exception("Wrong parameters quantity into creating Circle");
	}

	const int id = ConvertToInt(commandLine[2]);
	const Color color(ConvertToInt(commandLine[3]), ConvertToInt(commandLine[4]), ConvertToInt(commandLine[5]));
	const Point point(ConvertToDouble(commandLine[6]), ConvertToDouble(commandLine[7]));
	const double radius = ConvertToDouble(commandLine[8]);
	
	AddShapeInProject(std::make_unique<Circle>(Circle(id, color, point, radius)));

}

void Interpreter::CreateTriangle(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 10)
	{
		throw std::exception("Wrong parameters quantity into creating Triangle");
	}

	const int id = ConvertToInt(commandLine[2]);
	const Color color(ConvertToInt(commandLine[3]), ConvertToInt(commandLine[4]), ConvertToInt(commandLine[5]));
	const Point point(ConvertToDouble(commandLine[6]), ConvertToDouble(commandLine[7]));
	const double width = ConvertToDouble(commandLine[8]);
	const double hight = ConvertToDouble(commandLine[9]);
	
	AddShapeInProject(std::make_unique<Triangle>(Triangle(id, color, point, width, hight)));
}

void Interpreter::CreateRectangle(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 10)
	{
		throw std::exception("Wrong parameters quantity into creating Rectangle");
	}

	const int id = ConvertToInt(commandLine[2]);
	const Color color(ConvertToInt(commandLine[3]), ConvertToInt(commandLine[4]), ConvertToInt(commandLine[5]));
	const Point point(ConvertToDouble(commandLine[6]), ConvertToDouble(commandLine[7]));
	const double width = ConvertToDouble(commandLine[8]);
	const double hight = ConvertToDouble(commandLine[9]);
	
	AddShapeInProject(std::make_unique<Rectangle>(Rectangle(id, color, point, width, hight)));
}

void Interpreter::CreateSquare(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 9)
	{
		throw std::exception("Wrong parameters quantity into creating Square");
	}

	const int id = ConvertToInt(commandLine[2]);
	const Color color(ConvertToInt(commandLine[3]), ConvertToInt(commandLine[4]), ConvertToInt(commandLine[5]));
	const Point point(ConvertToDouble(commandLine[6]), ConvertToDouble(commandLine[7]));
	const double width = ConvertToDouble(commandLine[8]);

	AddShapeInProject(std::make_unique<Square>(Square(id, color, point, width)));
}

auto Interpreter::FindShapeWithId(int id) const
{
	auto iterator = idShapeMap.find(id);

	if (iterator == idShapeMap.end())
	{
		throw std::exception("Shape with id witch you going to modifier does not exist");
	}

	return iterator;
}

void Interpreter::SetColor(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 6)
	{
		throw std::exception("Wrong parameters quantity into set color");
	}
	else if (commandLine[1] != "color")
	{
		throw std::exception("Wrong command into set color");
	}

	const int id = ConvertToInt(commandLine[2]);
	const Color color(ConvertToInt(commandLine[3]), ConvertToInt(commandLine[4]), ConvertToInt(commandLine[5]));

	const auto it = FindShapeWithId(id);
	const auto listElement = it->second;
	auto* shape = listElement->get();
	shape->SetColor(color);
}

void Interpreter::Move(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 4)
	{
		throw std::exception("Wrong parameters quantity into move command");
	}

	const int id = ConvertToInt(commandLine[1]);
	const double dx = ConvertToDouble(commandLine[2]);
	const double dy = ConvertToDouble(commandLine[3]);

	const auto it = FindShapeWithId(id);
	const auto listElement = it->second;
	auto* shape = listElement->get();
	shape->MoveShape(dx, dy);
}

void Interpreter::Scale(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 3)
	{
		throw std::exception("Wrong parameters quantity into scale command");
	}

	const int id = ConvertToInt(commandLine[1]);
	const double scale = ConvertToDouble(commandLine[2]);

	const auto it = FindShapeWithId(id);
	const auto listElement = it->second;
	auto* shape = listElement->get();
	shape->Scale(scale);
}

void Interpreter::BringTo(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 4)
	{
		throw std::exception("Wrong parameters quantity into bring to");
	}
	else if (commandLine[1] != "to")
	{
		throw std::exception("Wrong command bring to");
	}

	const int id = ConvertToInt(commandLine[3]);
	
	const auto it = FindShapeWithId(id);
	const auto listIterator = it->second;

	std::unique_ptr<Shape> tempShape = std::move(*listIterator);

	idShapeMap.erase(id);
	shapes.erase(listIterator);

	if (commandLine[2] == "front")
	{
		shapes.push_back(std::move(tempShape));
		idShapeMap.emplace(id, --shapes.end());
	}
	else if (commandLine[2] == "back")
	{
		shapes.push_front(std::move(tempShape));
		idShapeMap.emplace(id, shapes.begin());
	}
	else
	{
		throw std::exception("Wrong command bring to");
	}
}

void Interpreter::Draw(const std::vector<std::string>& commandLine)
{
	if (commandLine.size() != 8)
	{
		throw std::exception("Wrong parameters quantity into draw");
	}

	const std::string outputFileName = commandLine[1];
	const Point startPoint(ConvertToDouble(commandLine[2]), ConvertToDouble(commandLine[3]));

	const double length_x = ConvertToDouble(commandLine[4]);
	const double length_y = ConvertToDouble(commandLine[5]);

	const int width = ConvertToInt(commandLine[6]);
	const int hight = ConvertToInt(commandLine[7]);

	Scene scene(width, hight, startPoint, length_x, length_y);

	for (auto& shape : shapes)
	{
		scene.AddShapeOnScene(*shape);
	}

	std::ofstream out(outputFileName);

	scene.DrawImage(out);
}

void Interpreter::Run()
{
	while (parser)
	{
		ExecuteCommand(parser.GetNextCommand());
	}
}

void Interpreter::AddShapeInProject(std::unique_ptr<Shape>&& shape)
{
	int id = shape->GetId();

	if (idShapeMap.find(id) != idShapeMap.end())
	{
		throw std::exception("Id booked");
	}

	shapes.emplace_back(std::move(shape));
	std::pair<int, std::list<std::unique_ptr<Shape>>::iterator> pair = std::make_pair(id, --shapes.end());
	idShapeMap.emplace(pair);
}

int Interpreter::ConvertToInt(const std::string& stringWithNumber) const
{
	auto* const check = new size_t;
	const int number = std::stoi(stringWithNumber, check);

	if (*check != stringWithNumber.size())
	{
		delete check;
		throw std::exception("Wrong number. Expected clear int.");
	}

	delete check;
	return number;
}

double Interpreter::ConvertToDouble(const std::string& stringWithNumber) const
{
	auto* const check = new size_t;
	const double number = std::stod(stringWithNumber, check);

	if (*check != stringWithNumber.size())
	{
		delete check;
		throw std::exception("Wrong number. Expected clear double.");
	}

	delete check;
	return number;
}


Interpreter::Interpreter(std::istream& inputStream) : parser(Parser(inputStream)) {}
