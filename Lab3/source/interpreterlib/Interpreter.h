#pragma once

#include <vector>
#include <string>
#include <fstream>
#include "Shape.h"
#include <list>
#include <memory>
#include <unordered_map>
#include "Parser.h"
#include <iterator>

class Interpreter
{
private:
	std::list<std::unique_ptr<Shape>> shapes;
	std::unordered_map<int, std::list<std::unique_ptr<Shape>>::iterator> idShapeMap;
	Parser parser;
	void ExecuteCommand(const std::vector<std::string>& commandLine);
	void CreateShape(const std::vector<std::string>& commandLine);
	void CreateCircle(const std::vector<std::string>& commandLine);
	void CreateTriangle(const std::vector<std::string>& commandLine);
	void CreateRectangle(const std::vector<std::string>& commandLine);
	void CreateSquare(const std::vector<std::string>& commandLine);
	void SetColor(const std::vector<std::string>& commandLine);
	void Move(const std::vector<std::string>& commandLine);
	void Scale(const std::vector<std::string>& commandLine);
	void BringTo(const std::vector<std::string>& commandLine);
	void Draw(const std::vector<std::string>& commandLine);
	void AddShapeInProject(std::unique_ptr<Shape>&& shape);
	int ConvertToInt(const std::string& stringWithNumber) const;
	double ConvertToDouble(const std::string& stringWithNumber) const;
	auto FindShapeWithId(int id) const;
public:
	Interpreter(std::istream& inputStream);
	void Run();
};

