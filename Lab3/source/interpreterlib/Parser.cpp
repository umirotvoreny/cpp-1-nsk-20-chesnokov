#include "Parser.h"

#include<fstream>
#include <sstream>

Parser::Parser(std::istream& inputStream) : stream(inputStream) {}

std::vector<std::string>& Parser::GetNextCommand()
{	
	std::string s;
	getline(stream, s);
	std::stringstream ss(s);
	command = std::vector<std::string>(std::istream_iterator<std::string>{ss}, std::istream_iterator<std::string>());
	return command;
}

Parser::operator bool() const
{
	return bool(stream);
}



