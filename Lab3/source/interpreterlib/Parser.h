#pragma once
#include <fstream>
#include <vector>


class Parser
{
private:
	std::istream& stream;
	std::vector<std::string> command;
public:
	Parser() = delete;
	Parser(std::istream& inputStream);
	std::vector<std::string>& GetNextCommand();
	operator bool() const;
};

