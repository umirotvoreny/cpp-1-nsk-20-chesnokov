#pragma once

class Point
{
private:
	double x = 0;
	double y = 0;
	
public:
	Point() = default;
	Point(double x, double y);
	double GetX() const;
	double GetY() const;
	void MovePoint(double dx, double dy);
};

