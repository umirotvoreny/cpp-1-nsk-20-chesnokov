#include "Rectangle.h"

Rectangle::Rectangle(int id, Color color, Point point, double widht, double hight) : Shape(id, color, point), width(widht), hight(hight)
{
	if (widht < 0 || hight < 0)
	{
		throw std::exception("Rectangles parameters should pe positive");
	}
}

void Rectangle::Scale(double scale)
{
	if (scale < 0)
	{
		throw std::exception("Wrong scale size");
	}
	
	const double newWidht = width * scale;
	const double newHight = hight * scale;

	MoveShape((width - newWidht) / 2, (hight - newHight) / 2);

	width = newWidht;
	hight = newHight;
}

bool Rectangle::ContainsPoint(Point otherPoint) const
{
	return otherPoint.GetX() >= GetPoint().GetX() &&
		otherPoint.GetX() <= GetPoint().GetX() + width &&
		otherPoint.GetY() >= GetPoint().GetY() &&
		otherPoint.GetY() <= GetPoint().GetY() + hight;
}
