#include "Scene.h"

Scene::Scene(unsigned int widht, unsigned int hight, Point startPoint, double lenghtX, double lenghtY): startPoint(startPoint), stepX(lenghtX/widht),stepY(lenghtY/hight)
{
	if (widht <= 0 || hight <= 0 || lenghtX <= 0 || lenghtY <= 0)
	{
		throw std::exception("Wrong Scene parameters");
	}

	this->startPoint.MovePoint(0, lenghtY);
	scene.resize(hight);

	for (int i = 0; i < scene.size(); ++i)
	{
		scene[i].resize(widht, Color(0, 0, 0));
	}
}

void Scene::AddShapeOnScene(const Shape& shape)
{	
	for (int i = 0; i < scene.size(); ++i)
	{
		for(int j = 0; j < scene[0].size(); ++j)
		{
			Point curPoint = startPoint;
			curPoint.MovePoint(stepX * j, -(stepY * i));
			if (shape.ContainsPoint(curPoint))
			{
				scene[i][j] = shape.GetColor();
			}
		}
	}
}

void Scene::DrawImage(std::ostream& outputStream) const
{
	outputStream << "P3" << std::endl;
	outputStream << scene[0].size() << " " << scene.size() << std::endl;
	outputStream << 255 << std::endl;

	for (int i = 0; i < scene.size(); ++i)
	{
		for (int j = 0; j < scene[0].size(); ++j)
		{
			outputStream << scene[i][j].GetRed() << " " << scene[i][j].GetGreen() << " " << scene[i][j].GetBlue() << " ";
		}
	}
}
