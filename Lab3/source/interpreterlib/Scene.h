#pragma once

#include <fstream>
#include "Color.h"
#include <vector>
#include "Point.h"
#include "Shape.h"

class Scene
{
private:
	std::vector<std::vector<Color>> scene;
	double stepX;
	double stepY;
	Point startPoint;
public:
	Scene(unsigned int widht, unsigned int hight, Point startPoint, double lenghtX, double lenghtY);
	void AddShapeOnScene(const Shape& shape);
	void DrawImage(std::ostream& outputStream) const;
};

