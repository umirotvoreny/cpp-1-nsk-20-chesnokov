#include "Shape.h"
#include <exception>

Shape::Shape(int id, Color color, Point point) : id(id), color(color), point(point){}

void Shape::SetColor(Color color)
{
	this->color = color;
}

int Shape::GetId() const
{
	return id;
}

void Shape::MoveShape(double dx, double dy)
{
	point.MovePoint(dx, dy);
}

Point Shape::GetPoint() const
{
	return point;
}

Color Shape::GetColor() const
{
	return color;
}


