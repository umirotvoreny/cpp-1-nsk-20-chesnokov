#pragma once

#include "Color.h"
#include "Point.h"

class Shape
{
private:
	int id;
	Color color;
	Point point;

protected:
	Shape(int id, Color color, Point point);
	
public:
	Shape() = default;
	void SetColor(Color color);
	int GetId() const;
	void MoveShape(double dx, double dy);
	virtual void Scale(double scale) = 0;
	virtual bool ContainsPoint(Point point) const = 0;
	Point GetPoint() const;
	Color GetColor() const;
	virtual ~Shape() = default;
};

