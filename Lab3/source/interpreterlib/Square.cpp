#include "Square.h"

Square::Square(int id, Color color, Point point, double width) : Shape(id, color, point), width(width)
{
	if (width < 0)
	{
		throw std::exception("Squares parameters should pe positive");
	}
}

void Square::Scale(double scale)
{
	if (scale < 0)
	{
		throw std::exception("Wrong scale size");
	}
	
	const double newWidht = width * scale;

	double d = (width - newWidht) / 2;

	MoveShape(d, d);

	width = newWidht;
}

bool Square::ContainsPoint(Point otherPoint) const
{
	return otherPoint.GetX() >= GetPoint().GetX() &&
		otherPoint.GetX() <= GetPoint().GetX() + width &&
		otherPoint.GetY() >= GetPoint().GetY() &&
		otherPoint.GetY() <= GetPoint().GetY() + width;
}
