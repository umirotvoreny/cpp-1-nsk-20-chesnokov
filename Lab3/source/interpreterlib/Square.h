#pragma once

#include <exception>
#include "Shape.h"

class Square : public Shape
{
private:
	double width;
	
public:
	Square() = default;
	Square(int id, Color color, Point point, double width);
	void Scale(double scale) override;
	bool ContainsPoint(Point otherPoint) const override;
};

