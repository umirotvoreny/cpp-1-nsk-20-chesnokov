#include "Triangle.h"

Triangle::Triangle(int id, Color color, Point point, double widht, double hight) : Shape(id, color, point), width(widht), hight(hight)
{
	if (widht < 0 || hight < 0)
	{
		throw std::exception("Triangles parameters should pe positive");
	}
}

void Triangle::Scale(double scale)
{
	if (scale < 0)
	{
		throw std::exception("Wrong scale size");
	}
	
	const double newWidht = width * scale;
	const double newHight = hight * scale;
	
	double dx = (width - newWidht) / 3;
	double dy = (hight - newHight) / 3;

	MoveShape(dx, dy);

	width = newWidht;
	hight = newHight;
}

bool Triangle::ContainsPoint(Point otherPoint) const
{
	return otherPoint.GetX() >= GetPoint().GetX() &&
		otherPoint.GetY() >= GetPoint().GetY() &&
		otherPoint.GetY() <= -(hight / width) * otherPoint.GetX() + GetPoint().GetY() + hight + (hight / width) * GetPoint().GetX();
}
