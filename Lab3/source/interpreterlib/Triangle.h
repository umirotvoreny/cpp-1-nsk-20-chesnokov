#pragma once

#include <exception>
#include"Shape.h"

class Triangle : public Shape
{
private:
	double width;
	double hight;

public:
	Triangle() = default;
	Triangle(int id, Color color, Point point, double widht, double hight);
	void Scale(double scale) override;
	bool ContainsPoint(Point otherPoint) const override;
};

