#include <iostream>
#include "interpreterlib/Interpreter.h"

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cerr << "Wrong parameters";
		return 1;
	}
	
	try
	{
		std::string pathToConfig = argv[1];

		std::ifstream inputFile(pathToConfig);

		if (!inputFile)
		{
			std::cerr << "Wrong path to config file";
			return 2;
		}

		Interpreter interpreter(inputFile);
		interpreter.Run();
	}
	catch (const std::exception e)
	{
		std::cerr << e.what();
		return 3;
	}

	return 0;
}