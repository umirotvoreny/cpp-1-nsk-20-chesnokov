#include "gtest/gtest.h"

#include "../interpreterlib/Interpreter.h"
#include "../interpreterlib/Shape.h"
#include "../interpreterlib/Color.h"
#include "../interpreterlib/Circle.h"
#include "../interpreterlib/Rectangle.h"
#include "../interpreterlib/Square.h"
#include "../interpreterlib/Triangle.h"
#include "../interpreterlib/Point.h"
#include "../interpreterlib/Scene.h"
#include "../interpreterlib/Parser.h"
#include <memory>
#include <sstream>
#include <fstream>

TEST(CreatingShape, Circle)
{
	int id = 1;
	double x = 100;
	double y = 100;
	double radius = 50;
	Point point(x,y);
	Color color(255,0,0);
	Circle circle(id, color, point, radius);

	Point point1(x + 35, y + 36);
	Point point2(x + 35, y + 35);
	Point point3(x - 35, y + 36);
	Point point4(x - 35, y + 35);
	Point point5(x - 35, y - 36);
	Point point6(x - 35, y - 35);
	Point point7(x + 35, y - 36);
	Point point8(x + 35, y - 35);
	Point point9(x + 50, y + 1);
	Point point10(x + 50, y + 0);
	Point point11(x + 1, y + 50);
	Point point12(x + 0, y + 50);

	EXPECT_EQ(false, circle.ContainsPoint(point1));
	EXPECT_EQ(true, circle.ContainsPoint(point2));

	EXPECT_EQ(false, circle.ContainsPoint(point3));
	EXPECT_EQ(true, circle.ContainsPoint(point4));

	EXPECT_EQ(false, circle.ContainsPoint(point5));
	EXPECT_EQ(true, circle.ContainsPoint(point6));

	EXPECT_EQ(false, circle.ContainsPoint(point7));
	EXPECT_EQ(true, circle.ContainsPoint(point8));

	EXPECT_EQ(false, circle.ContainsPoint(point9));
	EXPECT_EQ(true, circle.ContainsPoint(point10));

	EXPECT_EQ(false, circle.ContainsPoint(point11));
	EXPECT_EQ(true, circle.ContainsPoint(point12));
}

TEST(CreatingShape, Rectangle)
{
	int id = 1;
	double x = 100;
	double y = 100;
	double widht = 50;
	double hight = 100;
	Point point(x,y);
	Color color(255,0,0);
	Rectangle rectangle(id, color, point, widht, hight);

	Point point1(x - 1, y);
	Point point2(x, y - 1);
	Point point3(x + widht + 1, y);
	Point point4(x + widht, y - 1);
	Point point5(x - 1, y + hight);
	Point point6(x, y + hight + 1);
	Point point7(x + widht, y + hight + 1);
	Point point8(x + widht + 1, y + hight);
	Point point9(x + widht, y);
	Point point10(x, y + hight);
	Point point11(x + widht, y + hight);
	Point point12(x + widht/2, y + hight/2);

	EXPECT_EQ(false, rectangle.ContainsPoint(point1));
	EXPECT_EQ(false, rectangle.ContainsPoint(point2));
	EXPECT_EQ(false, rectangle.ContainsPoint(point3));
	EXPECT_EQ(false, rectangle.ContainsPoint(point4));
	EXPECT_EQ(false, rectangle.ContainsPoint(point5));
	EXPECT_EQ(false, rectangle.ContainsPoint(point6));
	EXPECT_EQ(false, rectangle.ContainsPoint(point7));
	EXPECT_EQ(false, rectangle.ContainsPoint(point8));
	
	EXPECT_EQ(true, rectangle.ContainsPoint(point));
	EXPECT_EQ(true, rectangle.ContainsPoint(point9));
	EXPECT_EQ(true, rectangle.ContainsPoint(point10));
	EXPECT_EQ(true, rectangle.ContainsPoint(point11));
	EXPECT_EQ(true, rectangle.ContainsPoint(point12));
}

TEST(CreatingShape, Square)
{
	int id = 1;
	double x = 100;
	double y = 100;
	double widht = 50;
	Point point(x,y);
	Color color(255,0,0);
	Square square(id, color, point, widht);

	Point point1(x - 1, y);
	Point point2(x, y - 1);
	Point point3(x + widht + 1, y);
	Point point4(x + widht, y - 1);
	Point point5(x - 1, y + widht);
	Point point6(x, y + widht + 1);
	Point point7(x + widht, y + widht + 1);
	Point point8(x + widht + 1, y + widht);
	Point point9(x + widht, y);
	Point point10(x, y + widht);
	Point point11(x + widht, y + widht);
	Point point12(x + widht/2, y + widht/2);

	EXPECT_EQ(false, square.ContainsPoint(point1));
	EXPECT_EQ(false, square.ContainsPoint(point2));
	EXPECT_EQ(false, square.ContainsPoint(point3));
	EXPECT_EQ(false, square.ContainsPoint(point4));
	EXPECT_EQ(false, square.ContainsPoint(point5));
	EXPECT_EQ(false, square.ContainsPoint(point6));
	EXPECT_EQ(false, square.ContainsPoint(point7));
	EXPECT_EQ(false, square.ContainsPoint(point8));
	
	EXPECT_EQ(true, square.ContainsPoint(point));
	EXPECT_EQ(true, square.ContainsPoint(point9));
	EXPECT_EQ(true, square.ContainsPoint(point10));
	EXPECT_EQ(true, square.ContainsPoint(point11));
	EXPECT_EQ(true, square.ContainsPoint(point12));
}

TEST(CreatingShape, Triangle)
{
	int id = 1;
	double x = 100;
	double y = 100;
	double widht = 50;
	double hight = 100;
	Point point(x,y);
	Color color(255,0,0);
	Triangle triangle(id, color, point, widht, hight);

	Point point1(x - 1, y);
	Point point2(x, y - 1);
	Point point3(x + widht - 1, y + 2.5);
	Point point4(x + widht, y - 1);
	Point point5(x - 1, y + hight);
	Point point6(x + 1, y + hight - 1);
	Point point7(x + widht/2, y + hight/2 + 1);
	Point point8(x + widht/2 + 1, y + hight/2);
	Point point9(x + widht, y);
	Point point10(x, y + hight);
	Point point11(x + widht/2, y + hight/2);
	Point point12(x + widht/2, y + hight/2 - 1);
	Point point13(x + widht/2 - 1, y + hight/2);

	EXPECT_EQ(false, triangle.ContainsPoint(point1));
	EXPECT_EQ(false, triangle.ContainsPoint(point2));
	EXPECT_EQ(false, triangle.ContainsPoint(point3));
	EXPECT_EQ(false, triangle.ContainsPoint(point4));
	EXPECT_EQ(false, triangle.ContainsPoint(point5));
	EXPECT_EQ(false, triangle.ContainsPoint(point6));
	EXPECT_EQ(false, triangle.ContainsPoint(point7));
	EXPECT_EQ(false, triangle.ContainsPoint(point8));
	
	EXPECT_EQ(true, triangle.ContainsPoint(point));
	EXPECT_EQ(true, triangle.ContainsPoint(point9));
	EXPECT_EQ(true, triangle.ContainsPoint(point10));
	EXPECT_EQ(true, triangle.ContainsPoint(point11));
	EXPECT_EQ(true, triangle.ContainsPoint(point12));
	EXPECT_EQ(true, triangle.ContainsPoint(point13));
}

TEST(MoveShape, Circle)
{
	int id = 1;
	double x = 0;
	double y = 0;
	Point point(x,y);
	double radius = 1;
	Point point1(0,0);
	Point point2(0,1);
	Point point3(1,0);
	Point point4(0,-1);
	Point point5(-1,0);
	Point point6(2,1);
	Point point7(1,2);
	Color color(255,0,0);
	Circle circle(id, color, point, radius);

	EXPECT_EQ(true, circle.ContainsPoint(point1));
	EXPECT_EQ(true, circle.ContainsPoint(point2));
	EXPECT_EQ(true, circle.ContainsPoint(point3));
	EXPECT_EQ(true, circle.ContainsPoint(point4));
	EXPECT_EQ(true, circle.ContainsPoint(point5));
	EXPECT_EQ(false, circle.ContainsPoint(point6));
	EXPECT_EQ(false, circle.ContainsPoint(point7));

	circle.MoveShape(1,1);


	EXPECT_EQ(false, circle.ContainsPoint(point1));
	EXPECT_EQ(true, circle.ContainsPoint(point2));
	EXPECT_EQ(true, circle.ContainsPoint(point3));
	EXPECT_EQ(false, circle.ContainsPoint(point4));
	EXPECT_EQ(false, circle.ContainsPoint(point5));
	EXPECT_EQ(true, circle.ContainsPoint(point6));
	EXPECT_EQ(true, circle.ContainsPoint(point7));
}

TEST(MoveShape, Square)
{
	int id = 1;
	double x = 0;
	double y = 0;
	Point point(x,y);
	double widht = 1;
	Point point1(0,0);
	Point point2(0,1);
	Point point3(1,0);
	Point point6(2,1);
	Point point7(1,2);
	Color color(255,0,0);
	Square square(id, color, point, widht);

	EXPECT_EQ(true, square.ContainsPoint(point1));
	EXPECT_EQ(true, square.ContainsPoint(point2));
	EXPECT_EQ(true, square.ContainsPoint(point3));
	EXPECT_EQ(false, square.ContainsPoint(point6));
	EXPECT_EQ(false, square.ContainsPoint(point7));

	square.MoveShape(1,1);

	EXPECT_EQ(false, square.ContainsPoint(point1));
	EXPECT_EQ(false, square.ContainsPoint(point2));
	EXPECT_EQ(false, square.ContainsPoint(point3));
	EXPECT_EQ(true, square.ContainsPoint(point6));
	EXPECT_EQ(true, square.ContainsPoint(point7));
}

TEST(MoveShape, Rectangle)
{
	int id = 1;
	Point point(0,0);
	double widht = 1;
	double hight = 2;
	Color color(255,0,0);
	Rectangle rectangle(id, color, point, widht, hight);

	Point point1(0,0);
	Point point2(0,2);
	Point point3(1,0);
	Point point4(1,2);
	Point point5(3,3);
	Point point6(3,5);
	Point point7(4,3);
	Point point8(4,5);


	EXPECT_EQ(true, rectangle.ContainsPoint(point1));
	EXPECT_EQ(true, rectangle.ContainsPoint(point2));
	EXPECT_EQ(true, rectangle.ContainsPoint(point3));
	EXPECT_EQ(true, rectangle.ContainsPoint(point4));
	EXPECT_EQ(false, rectangle.ContainsPoint(point5));
	EXPECT_EQ(false, rectangle.ContainsPoint(point6));
	EXPECT_EQ(false, rectangle.ContainsPoint(point7));
	EXPECT_EQ(false, rectangle.ContainsPoint(point8));

	rectangle.MoveShape(3,3);

	EXPECT_EQ(false, rectangle.ContainsPoint(point1));
	EXPECT_EQ(false, rectangle.ContainsPoint(point2));
	EXPECT_EQ(false, rectangle.ContainsPoint(point3));
	EXPECT_EQ(false, rectangle.ContainsPoint(point4));
	EXPECT_EQ(true, rectangle.ContainsPoint(point5));
	EXPECT_EQ(true, rectangle.ContainsPoint(point6));
	EXPECT_EQ(true, rectangle.ContainsPoint(point7));
	EXPECT_EQ(true, rectangle.ContainsPoint(point8));
}

TEST(MoveShape, Triangle)
{
	int id = 1;
	Point point(0,0);
	double widht = 1;
	double hight = 2;
	Color color(255,0,0);
	Triangle triangle(id, color, point, widht, hight);

	Point point1(0,0);
	Point point2(0,2);
	Point point3(1,0);
	Point point4(1,2);
	Point point5(3,3);
	Point point6(3,5);
	Point point7(4,3);
	Point point8(4,5);


	EXPECT_EQ(true, triangle.ContainsPoint(point1));
	EXPECT_EQ(true, triangle.ContainsPoint(point2));
	EXPECT_EQ(true, triangle.ContainsPoint(point3));
	EXPECT_EQ(false, triangle.ContainsPoint(point4));
	EXPECT_EQ(false, triangle.ContainsPoint(point5));
	EXPECT_EQ(false, triangle.ContainsPoint(point6));
	EXPECT_EQ(false, triangle.ContainsPoint(point7));
	EXPECT_EQ(false, triangle.ContainsPoint(point8));

	triangle.MoveShape(3,3);

	EXPECT_EQ(false, triangle.ContainsPoint(point1));
	EXPECT_EQ(false, triangle.ContainsPoint(point2));
	EXPECT_EQ(false, triangle.ContainsPoint(point3));
	EXPECT_EQ(false, triangle.ContainsPoint(point4));
	EXPECT_EQ(true, triangle.ContainsPoint(point5));
	EXPECT_EQ(true, triangle.ContainsPoint(point6));
	EXPECT_EQ(true, triangle.ContainsPoint(point7));
	EXPECT_EQ(false, triangle.ContainsPoint(point8));
}

TEST(Scale, Circle)
{
	int id = 1;
	Point point(0,0);
	double radius = 1;
	Color color(255,0,0);
	Circle circle(id, color, point, radius);

	Point point1(0,0);
	Point point2(0,1);
	Point point3(1,0);
	Point point4(0,-1);
	Point point5(-1,0);
	Point point6(0,2);
	Point point7(2,0);
	Point point8(0,-2);
	Point point9(-2,0);

	EXPECT_EQ(true, circle.ContainsPoint(point1));
	EXPECT_EQ(true, circle.ContainsPoint(point2));
	EXPECT_EQ(true, circle.ContainsPoint(point3));
	EXPECT_EQ(true, circle.ContainsPoint(point4));
	EXPECT_EQ(true, circle.ContainsPoint(point5));
	EXPECT_EQ(false, circle.ContainsPoint(point6));
	EXPECT_EQ(false, circle.ContainsPoint(point7));
	EXPECT_EQ(false, circle.ContainsPoint(point8));
	EXPECT_EQ(false, circle.ContainsPoint(point9));

	circle.Scale(2);

	EXPECT_EQ(true, circle.ContainsPoint(point1));
	EXPECT_EQ(true, circle.ContainsPoint(point2));
	EXPECT_EQ(true, circle.ContainsPoint(point3));
	EXPECT_EQ(true, circle.ContainsPoint(point4));
	EXPECT_EQ(true, circle.ContainsPoint(point5));
	EXPECT_EQ(true, circle.ContainsPoint(point6));
	EXPECT_EQ(true, circle.ContainsPoint(point7));
	EXPECT_EQ(true, circle.ContainsPoint(point8));
	EXPECT_EQ(true, circle.ContainsPoint(point9));
}

TEST(Scale, Square)
{
	int id = 1;
	Point point(0,0);
	double widht = 1;
	Color color(255,0,0);
	Square square(id, color, point, widht);

	Point point1(0,0);
	Point point2(0,1);
	Point point3(1,0);
	Point point4(1,1);
	Point point5(-1,-1);
	Point point6(-1,2);
	Point point7(2,2);
	Point point8(2,-1);

	EXPECT_EQ(true, square.ContainsPoint(point1));
	EXPECT_EQ(true, square.ContainsPoint(point2));
	EXPECT_EQ(true, square.ContainsPoint(point3));
	EXPECT_EQ(true, square.ContainsPoint(point4));
	EXPECT_EQ(false, square.ContainsPoint(point5));
	EXPECT_EQ(false, square.ContainsPoint(point6));
	EXPECT_EQ(false, square.ContainsPoint(point7));
	EXPECT_EQ(false, square.ContainsPoint(point8));

	square.Scale(3);

	EXPECT_EQ(true, square.ContainsPoint(point1));
	EXPECT_EQ(true, square.ContainsPoint(point2));
	EXPECT_EQ(true, square.ContainsPoint(point3));
	EXPECT_EQ(true, square.ContainsPoint(point4));
	EXPECT_EQ(true, square.ContainsPoint(point5));
	EXPECT_EQ(true, square.ContainsPoint(point6));
	EXPECT_EQ(true, square.ContainsPoint(point7));
	EXPECT_EQ(true, square.ContainsPoint(point8));
}

TEST(Scale, Rectangle)
{
	int id = 1;
	Point point(0,0);
	double widht = 1;
	double hight = 2;
	Color color(255,0,0);
	Rectangle rectangle(id, color, point, widht, hight);

	Point point1(0,0);
	Point point2(0,2);
	Point point3(1,0);
	Point point4(1,2);
	Point point5(-1,-2);
	Point point6(-1,4);
	Point point7(2,-2);
	Point point8(2,4);


	EXPECT_EQ(true, rectangle.ContainsPoint(point1));
	EXPECT_EQ(true, rectangle.ContainsPoint(point2));
	EXPECT_EQ(true, rectangle.ContainsPoint(point3));
	EXPECT_EQ(true, rectangle.ContainsPoint(point4));
	EXPECT_EQ(false, rectangle.ContainsPoint(point5));
	EXPECT_EQ(false, rectangle.ContainsPoint(point6));
	EXPECT_EQ(false, rectangle.ContainsPoint(point7));
	EXPECT_EQ(false, rectangle.ContainsPoint(point8));

	rectangle.Scale(3);

	EXPECT_EQ(true, rectangle.ContainsPoint(point1));
	EXPECT_EQ(true, rectangle.ContainsPoint(point2));
	EXPECT_EQ(true, rectangle.ContainsPoint(point3));
	EXPECT_EQ(true, rectangle.ContainsPoint(point4));
	EXPECT_EQ(true, rectangle.ContainsPoint(point5));
	EXPECT_EQ(true, rectangle.ContainsPoint(point6));
	EXPECT_EQ(true, rectangle.ContainsPoint(point7));
	EXPECT_EQ(true, rectangle.ContainsPoint(point8));
}

TEST(Scale, Triangle)
{
	int id = 1;
	Point point(0,0);
	double widht = 1;
	double hight = 2;
	Color color(255,0,0);
	Triangle triangle(id, color, point, widht, hight);

	Point point1(0,0);
	Point point2(0,2);
	Point point3(1,0);
	Point point4(1,2);
	Point point5(-1,-1);
	Point point6(-1,5);
	Point point7(2,-1);
	Point point8(1,2);


	EXPECT_EQ(true, triangle.ContainsPoint(point1));
	EXPECT_EQ(true, triangle.ContainsPoint(point2));
	EXPECT_EQ(true, triangle.ContainsPoint(point3));
	EXPECT_EQ(false, triangle.ContainsPoint(point4));
	EXPECT_EQ(false, triangle.ContainsPoint(point5));
	EXPECT_EQ(false, triangle.ContainsPoint(point6));
	EXPECT_EQ(false, triangle.ContainsPoint(point7));
	EXPECT_EQ(false, triangle.ContainsPoint(point8));

	triangle.Scale(5);

	EXPECT_EQ(true, triangle.ContainsPoint(point1));
	EXPECT_EQ(true, triangle.ContainsPoint(point2));
	EXPECT_EQ(true, triangle.ContainsPoint(point3));
	EXPECT_EQ(true, triangle.ContainsPoint(point4));
	EXPECT_EQ(true, triangle.ContainsPoint(point5));
	EXPECT_EQ(true, triangle.ContainsPoint(point6));
	EXPECT_EQ(true, triangle.ContainsPoint(point7));
	EXPECT_EQ(true, triangle.ContainsPoint(point8));
}

TEST(Scene, Draw)
{
	int id = 1;
	Point point(1,1);
	double radius = 1;
	Color color(255,0,0);
	Circle circle(id, color, point, radius);
	Point scenePoint(0,0);
	Scene scene(3,3,scenePoint, 2,2);
	std::unique_ptr<Shape> shape = std::make_unique<Circle>(circle);
	scene.AddShapeOnScene(*shape);

	std::stringstream ss;
	scene.DrawImage(ss);

	std::stringstream expect;
	expect << "P3" << std::endl;
	expect << 3 << " " << 3 << std::endl;
	expect << 255 << std::endl;
	expect << "0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 0 0 0 255 0 0 255 0 0 ";

	EXPECT_EQ(expect.str(), ss.str());
}

TEST(Interpretator, Easy)
{
	std::stringstream input("create circle 1 255 0 0 1.5 1.5 1.5\ndraw out1.ppm 0 0 3 3 3 3");
	Interpreter inter(input);
	inter.Run();

	std::ifstream outputFile("out1.ppm");
	std::stringstream output;
	output << outputFile.rdbuf();

	std::stringstream expect("P3\n3 3\n255\n0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 0 0 0 255 0 0 255 0 0 ");

	EXPECT_EQ(expect.str(), output.str());
}

TEST(Interpretator, Hard)
{
	std::stringstream input("create circle 1  255 0 0 100 100 50.123\ncreate circle 11313 255 255 0 100.13 678 20\ncreate triangle 2 0 0 255 542 142 162 266\ncreate square 3 0 255 0 674 700 144\nmove 11313 300 -50\nbring to front 11313\nscale 2 4\nscale 1 6\nbring to front 1\ndraw out.ppm 0 0 900 900 40 40");
	Interpreter inter(input);
	inter.Run();

	std::ifstream outputFile("out.ppm");
	std::stringstream output;
	output << outputFile.rdbuf();

	std::stringstream expect("P3\n40 40\n255\n0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 0 255 255 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 ");

	EXPECT_EQ(expect.str(), output.str());
}